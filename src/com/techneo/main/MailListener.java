package com.techneo.main;

import java.io.IOException;
import java.util.TimerTask;

import com.techneo.mailHandler.SessionHandler;
import com.techneo.ui.ErrorMessageUIController;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/*
 * 
 * author: Gayathree Munasinghe
 * class name: MailListener
 * extends: TimerTask
 * action: scheduled task to read emails 
 * 
 */
public class MailListener extends TimerTask {
	private static MailListener instance = null;

	public static MailListener getInstance() {
		if (instance == null) {
			instance = new MailListener();
		}
		return instance;
	}

	@Override
	public void run() {
		try {
			SessionHandler.getInstance().startSessions();
		} catch (Exception ex) {
			FXMLLoader errorloader = new FXMLLoader(getClass().getResource("ErrorMessageUIFxml.fxml"));
			Stage errorstage = new Stage(StageStyle.DECORATED);
			errorstage.setResizable(false);
			errorstage.setTitle("Project Allocator-Error");
			try {
				errorstage.setScene(new Scene((Pane) errorloader.load()));
			} catch (IOException e) {
				e.printStackTrace();
				System.exit(1);
			}
			ErrorMessageUIController errorCntroller = errorloader.<ErrorMessageUIController> getController();
			errorCntroller.init("Error while listeneing for e-mails!", "Actual error: \n" + ex.toString());
			errorstage.show();
		}

	}
}

package com.techneo.main;

import com.techneo.ui.StarterUI;

/*
 * 
 * author: Gayathree Munasinghe
 * class name: Main
 * action: used to test the code written
 * 
 */

public class Main {
	public static void main(String args[]) {
		try {
			StarterUI.launch(StarterUI.class);
		} catch (Exception e) {
			System.exit(1);
		}
	}

}

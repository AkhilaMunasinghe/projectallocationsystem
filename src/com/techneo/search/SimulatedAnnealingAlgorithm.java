package com.techneo.search;

import com.techneo.CandidateAssignment;
import com.techneo.CandidateSolution;
import com.techneo.PreferenceTable;

/*
 * 
 * author: Gayathree Munasinghe
 * class name: SimulatedAnnealingAlgorithm
 * implements: SearchAlgorithm
 * action: generate the solution using simulated search algorithm
 * 
 */

public class SimulatedAnnealingAlgorithm implements SearchAlgorithm {

	@Override
	public CandidateSolution generateSolution(String filePath) throws Exception {
		double temperature = 1000;
		double coolingRate = 0.002;
		PreferenceTable preferences = new PreferenceTable(filePath);
		CandidateSolution initialSolution = new CandidateSolution(preferences);
		CandidateSolution bestSolution = initialSolution;
		while (temperature > 1) {
			// store initial solution energy
			int initialSolutonEnergy = initialSolution.getEnergy();
			// generate a random solution
			CandidateSolution currentSolution = new CandidateSolution(preferences);
			// get two random assignments
			CandidateAssignment assignmentOne = currentSolution.getRandomAssignment();
			CandidateAssignment assignmentTwo = currentSolution.getRandomAssignment();
			// swap the assigned projects
			int positionOne = currentSolution.getCandidateAssignmentList().indexOf(assignmentOne);
			int positionTwo = currentSolution.getCandidateAssignmentList().indexOf(assignmentTwo);
			String projectOne = assignmentOne.getProject();
			String projectTwo = assignmentTwo.getProject();
			assignmentOne.setProject(projectTwo);
			currentSolution.getCandidateAssignmentList().set(positionOne, assignmentOne);
			assignmentTwo.setProject(projectOne);
			currentSolution.getCandidateAssignmentList().set(positionTwo, assignmentOne);
			// get energy of current solution
			int currentSolutionEnergy = currentSolution.getEnergy();
			// check if the neighbour is acceptable
			if (this.calculateAcceptanceProbability(initialSolutonEnergy, currentSolutionEnergy, temperature) > Math
					.random()) {
				initialSolution = currentSolution;
			}
			// check if the solution is best solution
			if (currentSolutionEnergy < initialSolutonEnergy) {
				bestSolution = initialSolution;
			}
			temperature *= 1 - coolingRate;
		}
		return bestSolution;
	}

	/* function to calculate the acceptance probability */
	private double calculateAcceptanceProbability(int initialEnergy, int newEnergy, double temperature) {
		if (newEnergy < initialEnergy) {
			return 1.0;
		}
		return Math.exp((initialEnergy - newEnergy) / temperature);
	}

}

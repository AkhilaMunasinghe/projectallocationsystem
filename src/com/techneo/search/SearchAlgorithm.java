package com.techneo.search;

import com.techneo.CandidateSolution;

/*
 * 
 * author: Gayathree Munasinghe
 * interface name: Search 
 * action: interface to be implemented by the search algorithms used
 * 
 */

public interface SearchAlgorithm {

	/* generate and return the solution */
	public CandidateSolution generateSolution(String filePath) throws Exception;

}

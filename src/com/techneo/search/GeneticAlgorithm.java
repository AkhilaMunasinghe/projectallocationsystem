package com.techneo.search;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Random;

import com.techneo.CandidateAssignment;
import com.techneo.CandidateSolution;
import com.techneo.PreferenceTable;

/*
 * 
 * author: Gayathree Munasinghe
 * class name: GeneticAlgorithm
 * implements: SearchAlgorithm
 * action: generate the solution using genetic algorithm
 * 
 */
public class GeneticAlgorithm implements SearchAlgorithm {

	private static List<CandidateSolution> sampleSpace = new ArrayList<CandidateSolution>();

	private int sampleSpaceSize;

	private Random randomGenerate = new Random();

	private PreferenceTable preferenceTable;

	String filepath;

	@Override
	public CandidateSolution generateSolution(String filePath) throws Exception {
		this.filepath = filePath;
		this.preferenceTable = new PreferenceTable(this.filepath);
		// this.sampleSpaceSize =
		// this.preferenceTable.getAllStudentEntries().size();
		this.sampleSpaceSize = 10;
		sampleSpace = this.generateSampleSpace(10);
		// get the bestfit available in the sample space
		CandidateSolution finalSolution = this.generateFittest(this.sampleSpace);
		for (int i = 0; i < sampleSpaceSize; i++) {
			CandidateSolution parentOne = this.generateParents();
			CandidateSolution parentTwo = this.generateParents();
			CandidateSolution child = this.crossOver(parentOne, parentTwo);
			CandidateSolution mutated = this.mutate(child);
			if (finalSolution.getEnergy() >= mutated.getEnergy()) {
				finalSolution = mutated;
			}
		}
		return finalSolution;
	}

	/* generate a samplespace */
	private List<CandidateSolution> generateSampleSpace(int size) throws IOException {
		List<CandidateSolution> space = new ArrayList<CandidateSolution>();
		for (int i = 0; i < size; i++) {
			PreferenceTable table = new PreferenceTable(this.filepath);
			CandidateSolution solution = new CandidateSolution(table);
			space.add(solution);
		}
		return space;
	}

	/* get the best solution available in the sample space */
	private CandidateSolution generateFittest(List<CandidateSolution> space) {
		CandidateSolution solution = space.get(0);
		for (CandidateSolution candidateSolution : space) {
			if (solution.getEnergy() >= candidateSolution.getEnergy()) {
				solution = candidateSolution;
			}
		}
		return solution;
	}

	/* create a cross between two parent solutions and return */
	private CandidateSolution crossOver(CandidateSolution parentOne, CandidateSolution parentTwo) {
		CandidateSolution child = new CandidateSolution(parentOne.getCandidateAssignmentList().size());
		int crossOverStartPoint = randomGenerate.nextInt(parentOne.getCandidateAssignmentList().size());
		int crossOverEndPoint = randomGenerate.nextInt(parentOne.getCandidateAssignmentList().size());
		if (crossOverEndPoint < crossOverStartPoint) {
			int changer = crossOverStartPoint;
			crossOverStartPoint = crossOverEndPoint;
			crossOverEndPoint = changer;
		}
		// apply assignments of parentOne to child
		for (int i = crossOverStartPoint; i < crossOverEndPoint; i++) {
			child.getCandidateAssignmentList().set(i, parentOne.getCandidateAssignmentList().get(i));
		}
		// apply assignments of parent two to child
		for (int i = 0; i < child.getCandidateAssignmentList().size(); i++) {
			if (child.getCandidateAssignmentList().get(i) == null) {
				child.getCandidateAssignmentList().set(i, parentTwo.getCandidateAssignmentList().get(i));
			}
		}
		return child;
	}

	/* generate parent required for crossover */
	private CandidateSolution generateParents() throws IOException {
		int spaceSize = this.randomGenerate.nextInt(sampleSpaceSize);
		while (spaceSize == 0 || spaceSize > this.sampleSpaceSize) {
			spaceSize = this.randomGenerate.nextInt(sampleSpaceSize);
		}
		int randomNumber = randomGenerate.nextInt(sampleSpaceSize);
		while (randomNumber == 0) {
			randomNumber = randomGenerate.nextInt(sampleSpaceSize);
		}
		List<CandidateSolution> space = this.generateSampleSpace(randomNumber);
		return this.generateFittest(space);
	}

	/* mutate the solution */
	private CandidateSolution mutate(CandidateSolution child) {
		List<String> projectList = new ArrayList<>();
		for (int i = 0; i < child.getCandidateAssignmentList().size(); i++) {
			CandidateAssignment assignment = child.getCandidateAssignmentList().get(i);
			if (!assignment.getStudent().isHasPreAssignedProject()) {
				if (projectList.contains(assignment.getProject())) {
					assignment.randomizeAssignment();
					child.getCandidateAssignmentList().set(i, assignment);
				} else {
					projectList.add(assignment.getProject());
				}
			} else {
				projectList.add(assignment.getProject());
			}
		}
		return child;
	}

}

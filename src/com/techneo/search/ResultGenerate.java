package com.techneo.search;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.techneo.CandidateAssignment;
import com.techneo.CandidateSolution;
import com.techneo.PreferenceTable;
import com.techneo.StudentEntry;
import com.techneo.mailHandler.SessionData;

/*
 * 
 * author: Gayathree Munasinghe
 * class name: ResultGenerate
 * action: generate the results based on given input path, save data to given output file and send emails to students
 * 
 */

public class ResultGenerate {

	private static final String COMMA = ",";

	private String inputFilePath;

	private String outputFolderPath;

	private String currentDateTime;

	public String getInputFilePath() {
		return inputFilePath;
	}

	public void setInputFilePath(String inputFilePath) {
		this.inputFilePath = inputFilePath;
	}

	public String getOutputFilePath() {
		return outputFolderPath;
	}

	public void setOutputFilePath(String outputFilePath) {
		this.outputFolderPath = outputFilePath;
	}

	public String getCurrentDateTime() {
		return currentDateTime;
	}

	public void setCurrentDateTime(String currentDateTime) {
		this.currentDateTime = currentDateTime;
	}

	public ResultGenerate() {

	}

	/* constructor of the class */
	public ResultGenerate(String inputPath, String outputPath) {
		this.inputFilePath = inputPath;
		this.outputFolderPath = outputPath + File.separator;
	}

	/*
	 * generate the results from data in the given file and save them separately
	 */
	public CandidateSolution[] generateResults() throws Exception {
		CandidateSolution[] outputs = new CandidateSolution[2];
		SearchAlgorithm genetic = new GeneticAlgorithm();
		CandidateSolution geneticSolution = genetic.generateSolution(this.inputFilePath);
		outputs[0] = geneticSolution;
		SearchAlgorithm simulated = new SimulatedAnnealingAlgorithm();
		CandidateSolution simulatedSolution = simulated.generateSolution(this.inputFilePath);
		outputs[1] = simulatedSolution;
		return outputs;
	}

	/* save the results in the given folder path */
	public static void saveResults(CandidateSolution solution, String outputPath) throws Exception {
		if (outputPath.charAt(outputPath.length() - 1) != File.separatorChar) {
			outputPath = outputPath + File.separator;
		}
		File folder = new File(outputPath);
		if (!folder.exists()) {
			folder.mkdir();
		}
		outputPath = outputPath + "Results.csv";
		FileOutputStream outputStream = new FileOutputStream(outputPath, false);
		OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream);
		BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWriter);
		String titleLine = "Student Name" + COMMA + "Project" + COMMA + "Peference Number" + COMMA + "Preassigned"
				+ COMMA + "Number of Preferences";
		bufferedWriter.write(titleLine);
		for (CandidateAssignment assignment : solution.getCandidateAssignmentList()) {
			StudentEntry student = assignment.getStudent();
			bufferedWriter.newLine();
			String line = student.getStudentName() + COMMA + assignment.getProject() + COMMA
					+ (student.getRanking(assignment.getProject()) + 1) + COMMA + student.isHasPreAssignedProject()
					+ COMMA + student.getNumberOfStatedPreferences();
			bufferedWriter.write(line);
		}
		bufferedWriter.flush();
		bufferedWriter.close();
		outputStreamWriter.close();
		outputStream.close();
	}

}

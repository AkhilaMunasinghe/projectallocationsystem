package com.techneo;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

import java.util.Random;

/*
 * 
 * author: Gayathree Munasinghe
 * class name: CandidateSolution
 * action: holds a list of CandidateAssignment objects
 * 
 */
public class CandidateSolution {

	private List<CandidateAssignment> candidateAssignmentList = new ArrayList<CandidateAssignment>();

	/* variable to store the penalty for violations */
	private int PENALTY = 1000;

	/* variable to store already assigned projects */
	private Hashtable<String, Integer> projectMapper = new Hashtable<>();

	private Hashtable<String, CandidateAssignment> projectStudentMap = new Hashtable<>();

	private Random RND = new Random();

	private List<StudentEntry> studentList = new ArrayList<>();

	public CandidateSolution() {
	}

	public CandidateSolution(int size) {
		for (int i = 0; i < size; i++) {
			this.candidateAssignmentList.add(null);
		}
	}

	/* construct a candidateSoltion object using preference table */
	public CandidateSolution(PreferenceTable preferenceTable) {
		for (StudentEntry student : preferenceTable.getAllStudentEntries()) {
			this.studentList.add(student);
			CandidateAssignment candidate = new CandidateAssignment(student);
			this.assignProject(candidate);
			// this.candidateAssignmentList.add(candidate);
		}
	}

	public List<CandidateAssignment> getCandidateAssignmentList() {
		return candidateAssignmentList;
	}

	public void setCandidateAssignmentList(List<CandidateAssignment> candidateAssignmentList) {
		this.candidateAssignmentList = candidateAssignmentList;
	}

	public CandidateAssignment getAssignmentFor(String studentName) {
		CandidateAssignment assignment = new CandidateAssignment();
		for (CandidateAssignment candidateAssignment : candidateAssignmentList) {
			if (candidateAssignment.getStudent().getStudentName().equalsIgnoreCase(studentName)) {
				assignment = candidateAssignment;
				break;
			}
		}
		return assignment;
	}

	/* return a random assignment of a random student */
	public CandidateAssignment getRandomAssignment() {
		return this.candidateAssignmentList.get(this.RND.nextInt(this.candidateAssignmentList.size()));
	}

	/* function to return the energy of the solutions */
	public int getEnergy() {
		this.projectMapper.clear();
		int sumOfEnergy = 0;
		for (CandidateAssignment candidateAssignment : candidateAssignmentList) {
			sumOfEnergy = sumOfEnergy + candidateAssignment.getEnergy();
			if (this.projectMapper.containsKey(candidateAssignment.getAssignedProject())) {
				this.projectMapper.replace(candidateAssignment.getAssignedProject(),
						this.projectMapper.get(candidateAssignment.getAssignedProject()) + 1);
			} else {
				this.projectMapper.put(candidateAssignment.getAssignedProject(), 1);
			}
		}
		Iterator<Entry<String, Integer>> iterator = projectMapper.entrySet().iterator();
		while (iterator.hasNext()) {
			Entry<String, Integer> currentEntry = iterator.next();
			if (currentEntry.getValue() > 1) {
				sumOfEnergy = sumOfEnergy + this.PENALTY * currentEntry.getValue();
			}
		}
		return sumOfEnergy;
	}

	/* function to return the fitness of the solution */
	public int getFitness() {
		return -(this.getEnergy());
	}

	/* function to assign a unique project to student */
	private void assignProject(CandidateAssignment assignment) {
		if (!projectStudentMap.containsKey(assignment.getAssignedProject())) {
			projectStudentMap.put(assignment.getAssignedProject(), assignment);
		} else {
			assignment = this.reAssignPreoject(assignment);
		}
		this.candidateAssignmentList.add(assignment);
	}

	/* function to assign a new project if already assigned */
	private CandidateAssignment reAssignPreoject(CandidateAssignment assignment) {
		// check if the student have a pre assigned project
		if (assignment.getStudent().isHasPreAssignedProject()) {
			// replace map with new assignment
			CandidateAssignment previous = this.projectStudentMap.get(assignment.getAssignedProject());
			this.projectStudentMap.replace(assignment.getProject(), previous, assignment);
			// change the old project(call this function)
			previous.randomizeAssignment();
			this.reAssignPreoject(previous);
		} else {
			// else
			boolean isAllocated = false;
			CandidateAssignment assignmentCopy = assignment;
			// try to randomize already given assignment
			for (int i = 0; i < 10; i++) {
				assignmentCopy.randomizeAssignment();
				if (!this.projectStudentMap.containsKey(assignmentCopy.getAssignedProject())) {
					isAllocated = true;
					assignment = assignmentCopy;
					this.projectStudentMap.put(assignment.getAssignedProject(), assignment);
					break;
				}
			}
			// if not yet allocated
			if (!isAllocated) {
				// check # of preferences
				if (assignment.getStudent().getNumberOfStatedPreferences() < 10) {
					// if #is less than 10
					assignmentCopy = assignment;
					while (this.projectStudentMap.containsKey(assignmentCopy.getAssignedProject())) {
						// assign a random project from a random assignment
						assignmentCopy.setProject(this.getRandomAssignment().getStudentEntry().getRandomPreference());
					}
					assignment = assignmentCopy;
				} else {
					// else
					// get previous assignment
					CandidateAssignment previousAssignment = this.projectStudentMap
							.get(assignment.getAssignedProject());
					// replace map
					this.projectStudentMap.replace(assignment.getAssignedProject(), previousAssignment, assignment);
					// randomize previous and call this function
					previousAssignment.randomizeAssignment();
					this.reAssignPreoject(previousAssignment);
				}
			}
		}
		return assignment;
	}

}

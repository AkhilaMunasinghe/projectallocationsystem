package com.techneo;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/*
 * 
 * author: Gayathree Munasinghe
 * class name: StudentEntry
 * action: store data about the student
 * 
 */
public class StudentEntry {
	private String studentName;
	private boolean hasPreAssignedProject;
	private List<String> orderedPreferences;
	private String projectAssigned;
	private int numberOfStatedPreferences;

	/* returns a String containing the name of this particular student */
	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	/* returns true if a project is pre-assigned */
	public boolean isHasPreAssignedProject() {
		return hasPreAssignedProject;
	}

	public void setHasPreAssignedProject(boolean hasPreAssignedProject) {
		this.hasPreAssignedProject = hasPreAssignedProject;
	}

	/*
	 * returns a list of project names (Strings) for which the student has
	 * expressed a preference, in order of preference
	 */
	public List<String> getOrderedPreferences() {
		return orderedPreferences;
	}

	public void setOrderedPreferences(List<String> orderedPreferences) {
		this.orderedPreferences = orderedPreferences;
		this.numberOfStatedPreferences = this.orderedPreferences.size();
	}

	/*
	 * ensures that a studentís list has only one preference, and records
	 * internally that thus preference has been pre-assigned
	 */
	public void preassignProject(String pname) {
		if (orderedPreferences.size() == 1 && this.hasPreAssignedProject) {
			this.projectAssigned = pname;
		}
	}

	/*
	 * returns an int indicating the original number of project preferences that
	 * were explicitly stated in the preference file.
	 */
	public int getNumberOfStatedPreferences() {
		return this.numberOfStatedPreferences;
	}

	/*
	 * assigns an extra project to the end of the studentís preferences list
	 * (without increasing the int returned by the method above)
	 */
	public void addProject(String pname) {
		if (!this.hasPreference(pname)) {
			this.orderedPreferences.add(pname.intern());
		}
	}

	/* get a random project preferred by the current student */
	public String getRandomPreference() {
		Random randomNumberGenerator = new Random();
		int randomNumber = randomNumberGenerator.nextInt(this.getNumberOfStatedPreferences());
		return this.getOrderedPreferences().get(randomNumber);
	}

	/* check if the given preference is already in the ordered preferences */
	public boolean hasPreference(String preference) {
		for (String givenPreference : this.getOrderedPreferences()) {
			if (givenPreference == preference) {
				return true;
			}
		}
		return false;
	}

	/* returns the ranking of the project */
	public int getRanking(String project) {
		return this.orderedPreferences.indexOf(project);
	}

}

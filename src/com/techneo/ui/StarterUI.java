package com.techneo.ui;

import java.util.Timer;

import com.techneo.main.MailListener;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;

/*
 * 
 * author: Gayathree Munasinghe
 * class name: StarterUI
 * extends: Application
 * action: Create a Starter UI and make it visible
 * 
 */
public class StarterUI extends Application {

	private Timer timer = new Timer();

	@Override
	public void start(Stage primaryStage) throws Exception {
		timer.schedule(MailListener.getInstance(), 0, 2000);
		Parent parent = FXMLLoader.load(getClass().getResource("StarterUIFxml.fxml"));
		Scene starterScene = new Scene(parent);
		primaryStage.setScene(starterScene);
		primaryStage.setTitle("Project Allocator-Main");
		primaryStage.setResizable(false);
		primaryStage.show();
		primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
			@Override
			public void handle(WindowEvent we) {
				closeApplication();
				System.exit(0);
			}
		});
	}

	private void closeApplication() {
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("ThakyouUIFxml.fxml"));
			Stage stage = new Stage(StageStyle.DECORATED);
			stage.setResizable(false);
			stage.setTitle("Project Allocator-Thankyou");
			stage.setScene(new Scene((Pane) loader.load()));
			stage.show();
			timer.cancel();
		} catch (Exception ex) {
			ex.printStackTrace();
			System.exit(1);
		}
	}

}

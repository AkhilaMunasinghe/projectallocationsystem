package com.techneo.ui;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;

/*
 * 
 * author: Gayathree Munasinghe
 * class name: SuccessMessageUIController
 * implements: Initializable
 * action: controller of the SuccessMessage UI
 * 
 */
public class SuccessMessageUIController implements Initializable {

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub

	}

	@FXML
	private Label lbl_SuccessMessage;

	@FXML
	private Button btn_OK;

	public void init(String message) {
		lbl_SuccessMessage.setText(message);
	}

	/* function performed when the ok button clicked. Close the message */
	public void okButtonClicked(ActionEvent event) {
		Stage stage = (Stage) btn_OK.getScene().getWindow();
		stage.close();
	}

}

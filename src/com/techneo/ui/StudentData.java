package com.techneo.ui;

/*
 * 
 * author: Gayathree Munasinghe
 * class name: StudentData
 * action: Store the student name and asssigned project. This class is to support the ResultUI
 * 
 */
public class StudentData {

	/**
	 * @param name
	 * @param project
	 */
	public StudentData(String name, String project, int preferenceNumber) {
		super();
		this.name = name;
		this.project = project;
		this.preferenceNumber = preferenceNumber;
	}

	private String name;

	private String project;

	private int preferenceNumber;

	public int getPreferenceNumber() {
		return preferenceNumber;
	}

	public void setPreferenceNumber(int preferenceNumber) {
		this.preferenceNumber = preferenceNumber;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getProject() {
		return project;
	}

	public void setProject(String project) {
		this.project = project;
	}

}

package com.techneo.ui;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.regex.Pattern;

import com.techneo.CandidateSolution;
import com.techneo.mailHandler.SendMail;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/*
 * 
 * author: Gayathree Munasinghe
 * class name: AddMailAddressFileUIController
 * implements: Initializable
 * action: controller of the AddMailAddressFileUI. prompts only in manual process 
 * 
 */
public class AddMailAddressFileUIController implements Initializable {
	private CandidateSolution solution;

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
	}

	@FXML
	private TextField txt_mailFile;

	@FXML
	private TextField txt_mailAddress;

	@FXML
	private PasswordField txt_password;

	@FXML
	Label lbl_eror;

	/* load the selected solution on UI start */
	public void init(CandidateSolution solution) {
		this.solution = solution;
	}

	/* function to validate the inputs */
	private boolean isValid() {
		lbl_eror.setText("");
		boolean valid = false;
		if (!txt_mailFile.getText().isEmpty() && !txt_mailAddress.getText().isEmpty()
				&& !txt_password.getText().isEmpty()) {
			if (txt_mailFile.getText().endsWith(".csv")) {
				File mailFile = new File(txt_mailFile.getText());
				if (mailFile.exists()) {
					if (Pattern
							.compile(
									"^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$")
							.matcher(txt_mailAddress.getText()).matches()) {
						valid = true;
					} else {
						lbl_eror.setText("Please enter a valid e-mail address");
					}
				} else {
					lbl_eror.setText("Mail address file does not exist");
				}
			} else {
				lbl_eror.setText("Please enter a .csv file as a mail address file");
			}
		} else {
			lbl_eror.setText("Please fill all fields");
		}
		return valid;
	}

	/* function performed on ok button click */
	public void okAction(ActionEvent event) throws IOException {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("PleaseWaitUIFxml.fxml"));
		Stage stage = new Stage(StageStyle.DECORATED);
		stage.setResizable(false);
		stage.setTitle("Project Allocator-Please wait...");
		try {
			if (this.isValid()) {
				stage.setScene(new Scene((Pane) loader.load()));
				stage.show();
				SendMail mail = new SendMail(txt_mailAddress.getText(), txt_password.getText());
				mail.mailResults(txt_mailFile.getText(), solution);
				stage.close();
				FXMLLoader successloader = new FXMLLoader(getClass().getResource("SuccessMessageFxml.fxml"));
				Stage successstage = new Stage(StageStyle.DECORATED);
				successstage.setResizable(false);
				successstage.setTitle("Project Allocator-Success");
				successstage.setScene(new Scene((Pane) successloader.load()));
				SuccessMessageUIController successcontroller = successloader
						.<SuccessMessageUIController> getController();
				successcontroller.init("E-mails sent successfully!");
				successstage.show();
			}
		} catch (Exception ex) {
			stage.close();
			FXMLLoader errorloader = new FXMLLoader(getClass().getResource("ErrorMessageUIFxml.fxml"));
			Stage errorstage = new Stage(StageStyle.DECORATED);
			errorstage.setResizable(false);
			errorstage.setTitle("Project Allocator-Error");
			errorstage.setScene(new Scene((Pane) errorloader.load()));
			ErrorMessageUIController errorCntroller = errorloader.<ErrorMessageUIController> getController();
			errorCntroller.init("Error while e-mailing results!", "Actual error: \n" + ex.toString());
			errorstage.show();
		}
	}

	/* function performed on cancel button click */
	public void cancelAction(ActionEvent event) {
		lbl_eror.setText("");
		txt_password.setText(null);
		txt_mailAddress.setText(null);
		txt_mailFile.setText(null);
	}

}

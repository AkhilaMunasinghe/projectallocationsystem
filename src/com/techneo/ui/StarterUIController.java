package com.techneo.ui;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Pattern;

import com.techneo.mailHandler.SendMail;
import com.techneo.mailHandler.SessionData;
import com.techneo.mailHandler.SessionDataHandler;
import com.techneo.mailHandler.SessionHandler;
import com.techneo.search.ResultGenerate;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/*
 * 
 * author: Gayathree Munasinghe
 * class name: StarterUIController
 * implements: Initializable
 * action: controller of the Starter UI
 * 
 */
public class StarterUIController implements Initializable {

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// this.checkCompletedActions();
		try {
			this.loadDataToStartUI();
		} catch (IOException ex) {
			try {
				FXMLLoader errorloader = new FXMLLoader(getClass().getResource("ErrorMessageUIFxml.fxml"));
				Stage errorstage = new Stage(StageStyle.DECORATED);
				errorstage.setResizable(false);
				errorstage.setTitle("Project Allocator-Error");
				errorstage.setScene(new Scene((Pane) errorloader.load()));
				ErrorMessageUIController errorCntroller = errorloader.<ErrorMessageUIController> getController();
				errorCntroller.init("Error while loading data!", "Actual error: \n" + ex.toString());
				errorstage.show();
			} catch (Exception ex1) {
				System.exit(1);
			}
		}
		list_completedSessions.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				try {
					selectCompletedAction(newValue);
				} catch (Exception ex) {
					System.exit(1);
				}
			}
		});
	}

	@FXML
	private TextField txt_inputFile;

	@FXML
	private TextField txt_outputFolder;

	@FXML
	private Button btn_generateManualResults;

	@FXML
	private Button btn_mainCancel;

	@FXML
	Button btn_viewData;

	@FXML
	private Label lbl_manualError;

	@FXML
	private Button btn_OK;

	@FXML
	private Label lbl_SuccessMessage;

	// function called from generate results button in starter ui
	public void generateResult(ActionEvent event) throws IOException {
		if (isValidInput()) {
			try {
				ResultGenerate resultGenerate = new ResultGenerate(txt_inputFile.getText(), txt_outputFolder.getText());
				FXMLLoader loader = new FXMLLoader(getClass().getResource("ResultUIFxml.fxml"));
				Stage stage = new Stage(StageStyle.DECORATED);
				stage.setResizable(false);
				stage.setTitle("Project Allocator-Results");
				stage.setScene(new Scene((Pane) loader.load()));
				ResultUIController controller = loader.<ResultUIController> getController();
				controller.init(resultGenerate.generateResults(), txt_outputFolder.getText(), null);
				stage.show();
			} catch (Exception ex) {
				ex.printStackTrace();
				FXMLLoader errorloader = new FXMLLoader(getClass().getResource("ErrorMessageUIFxml.fxml"));
				Stage errorstage = new Stage(StageStyle.DECORATED);
				errorstage.setResizable(false);
				errorstage.setTitle("Project Allocator-Error");
				errorstage.setScene(new Scene((Pane) errorloader.load()));
				ErrorMessageUIController errorCntroller = errorloader.<ErrorMessageUIController> getController();
				errorCntroller.init("Error while generating results!", "Actual error: \n" + ex.toString());
				errorstage.show();
			}

		}

	}

	private boolean isValidInput() {
		lbl_manualError.setText("");
		boolean valid = false;
		if (!txt_inputFile.getText().isEmpty() && !txt_outputFolder.getText().isEmpty()) {
			if (!txt_inputFile.getText().endsWith(".tsv")) {
				lbl_manualError.setText("Please enter a .tsv file as the input file");
			} else {
				File inputFile = new File(txt_inputFile.getText());
				if (!inputFile.exists()) {
					lbl_manualError.setText("Input file does not exist");
				} else {
					File outputFile = new File(txt_outputFolder.getText());
					if (!outputFile.isDirectory()) {
						lbl_manualError.setText("Either the output folder doesnot exist or not a directory");
					} else {
						valid = true;
					}
				}
			}
		} else {
			lbl_manualError.setText("Please enter input file path and output folder path");
		}
		return valid;
	}

	// function called by view data button
	public void viewLoadedData(ActionEvent event) throws IOException {
		if (isValidInput()) {
			try {
				Process p = Runtime.getRuntime()
						.exec("rundll32 url.dll,FileProtocolHandler " + txt_inputFile.getText());
				p.waitFor();
			} catch (Exception ex) {
				FXMLLoader errorloader = new FXMLLoader(getClass().getResource("ErrorMessageUIFxml.fxml"));
				Stage errorstage = new Stage(StageStyle.DECORATED);
				errorstage.setResizable(false);
				errorstage.setTitle("Project Allocator-Error");
				errorstage.setScene(new Scene((Pane) errorloader.load()));
				ErrorMessageUIController errorCntroller = errorloader.<ErrorMessageUIController> getController();
				errorCntroller.init("Error while opening data file!", "Actual error: \n" + ex.toString());
				errorstage.show();
			}
		}

	}

	// function called in cancel button in starter ui
	public void cancel(ActionEvent event) {
		txt_inputFile.setText("");
		txt_outputFolder.setText("");
	}

	// mail listener ui

	@FXML
	ListView<String> list_runningSessions;

	@FXML
	ListView<String> list_completedSessions;

	@FXML
	TextField txt_BatchID;

	@FXML
	TextField txt_mailFolder;

	@FXML
	TextField txt_mailTitle;

	@FXML
	TextField txt_mailAddress;

	@FXML
	TextField txt_password;

	@FXML
	TextField txt_deadline;

	@FXML
	TextField txt_mailAddressFile;

	@FXML
	TextField txt_dataFilePath;

	@FXML
	TextField txt_projectListFile;

	@FXML
	Label lbl_createSessionError;

	/* function to create a new session on "Create Session" button click */
	public void createNewSession(ActionEvent event) throws IOException {
		try {
			if (isValid()) {
				FXMLLoader loader = new FXMLLoader(getClass().getResource("PleaseWaitUIFxml.fxml"));
				Stage stage = new Stage(StageStyle.DECORATED);
				stage.setResizable(false);
				stage.setAlwaysOnTop(true);
				stage.setTitle("Project Allocator-Please wait...");
				stage.setScene(new Scene((Pane) loader.load()));
				stage.show();
				list_runningSessions.getItems().add(txt_BatchID.getText());
				SessionData newSessionDataObject = new SessionData(txt_BatchID.getText(), txt_mailFolder.getText(),
						txt_mailTitle.getText(), txt_mailAddress.getText(), txt_password.getText(),
						txt_deadline.getText(), "NEW", txt_mailAddressFile.getText(), txt_dataFilePath.getText(),
						txt_projectListFile.getText());
				SessionDataHandler.getInstance().addSessionData(newSessionDataObject);
				SendMail mailSender = new SendMail(newSessionDataObject.getMailAddress(),
						newSessionDataObject.getMailPassWord());
				mailSender.sendProjectReqestMail(newSessionDataObject);
				this.clear(event);
				stage.close();
			}
		} catch (Exception ex) {
			FXMLLoader errorloader = new FXMLLoader(getClass().getResource("ErrorMessageUIFxml.fxml"));
			Stage errorstage = new Stage(StageStyle.DECORATED);
			errorstage.setResizable(false);
			errorstage.setTitle("Project Allocator-Error");
			errorstage.setScene(new Scene((Pane) errorloader.load()));
			ErrorMessageUIController errorCntroller = errorloader.<ErrorMessageUIController> getController();
			errorCntroller.init("Error while creatibg new session!", "Actual error: \n" + ex.toString());
			errorstage.show();
		}
	}

	/* function to load data on ui start */
	private void loadDataToStartUI() throws IOException {
		List<SessionData> dataList = SessionDataHandler.getInstance().getSessionDataList();
		for (SessionData sessionData : dataList) {
			if (sessionData.getStatus().equalsIgnoreCase("completed")) {
				list_completedSessions.getItems().add(sessionData.getBatch());
			} else {
				list_runningSessions.getItems().add(sessionData.getBatch());
			}
		}
	}

	/* function to validate the mail session inputs */
	private boolean isValid() {
		boolean valid = false;
		lbl_createSessionError.setText("");
		if (!txt_BatchID.getText().isEmpty() && !txt_mailFolder.getText().isEmpty()
				&& !txt_mailTitle.getText().isEmpty() && !txt_mailAddress.getText().isEmpty()
				&& !txt_password.getText().isEmpty() && !txt_deadline.getText().isEmpty()
				&& !txt_mailAddressFile.getText().isEmpty() && !txt_dataFilePath.getText().isEmpty()
				&& !txt_projectListFile.getText().isEmpty()) {
			if (Pattern
					.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$")
					.matcher(txt_mailAddress.getText()).matches()) {
				if (txt_mailAddressFile.getText().endsWith(".csv")) {
					File mailAddressFile = new File(txt_mailAddressFile.getText());
					if (mailAddressFile.exists()) {
						File mailFolderPath = new File(txt_dataFilePath.getText());
						if (mailFolderPath.isDirectory()) {
							File projectListFile = new File(txt_projectListFile.getText());
							if (projectListFile.exists()) {
								if (txt_deadline.getText().matches("([0-9]{2})/([0-9]{2})/([0-9]{4})")) {
									valid = true;
								} else {
									lbl_createSessionError.setText("Deadline is not in the required format");
								}
							} else {
								lbl_createSessionError.setText("Project list file does not exist");
							}
						} else {
							lbl_createSessionError.setText("Either data folder does not exist or is not a folder");
						}
					} else {
						lbl_createSessionError.setText("Mail address file does not exist");
					}
				} else {
					lbl_createSessionError.setText("Please enter a .csv file as the mail address file");
				}
			} else {
				lbl_createSessionError.setText("Please enter a valid e-mail address");
			}
		} else {
			lbl_createSessionError.setText("Please fill all fields");
		}
		return valid;
	}

	/* function performed when clear button is clicked */
	public void clear(ActionEvent event) {
		txt_BatchID.setText("");
		txt_mailFolder.setText("");
		txt_mailTitle.setText("");
		txt_mailAddress.setText("");
		txt_mailAddressFile.setText("");
		txt_dataFilePath.setText("");
		txt_projectListFile.setText("");
		txt_password.setText("");
		txt_deadline.setText("");
	}

	public void selectCompletedAction(String sessionID) throws IOException {
		try {
			System.out.println("event: " + sessionID);
			List<SessionData> sessionDataList = SessionDataHandler.getInstance().getSessionDataList();
			SessionData selectedSession = new SessionData();
			for (SessionData sessionData : sessionDataList) {
				if (sessionData.getBatch().equalsIgnoreCase(sessionID)) {
					selectedSession = sessionData;
					break;
				}
			}
			ResultGenerate resultGenerate = new ResultGenerate(selectedSession.getDataFile() + File.separator
					+ selectedSession.getBatch() + "_ProjectAllocationData.tsv", selectedSession.getDataFile());
			FXMLLoader loader = new FXMLLoader(getClass().getResource("ResultUIFxml.fxml"));
			Stage stage = new Stage(StageStyle.DECORATED);
			stage.setResizable(false);
			stage.setTitle("Project Allocator-Results");
			stage.setScene(new Scene((Pane) loader.load()));
			ResultUIController controller = loader.<ResultUIController> getController();
			controller.init(resultGenerate.generateResults(), selectedSession.getDataFile(), selectedSession);
			stage.show();
		} catch (Exception ex) {
			FXMLLoader errorloader = new FXMLLoader(getClass().getResource("ErrorMessageUIFxml.fxml"));
			Stage errorstage = new Stage(StageStyle.DECORATED);
			errorstage.setResizable(false);
			errorstage.setTitle("Project Allocator-Error");
			errorstage.setScene(new Scene((Pane) errorloader.load()));
			ErrorMessageUIController errorCntroller = errorloader.<ErrorMessageUIController> getController();
			errorCntroller.init("Error while selecting session!", "Actual error: \n" + ex.toString());
			errorstage.show();
		}
	}

}

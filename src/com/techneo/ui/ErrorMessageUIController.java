package com.techneo.ui;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;

/*
 * 
 * author: Gayathree Munasinghe
 * class name: ErrorMessageUIController
 * implements: Initializable
 * action: controller of the ErrorMessage UI
 * 
 */
public class ErrorMessageUIController implements Initializable {

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {

	}

	@FXML
	private Label lbl_errorMsg;

	@FXML
	private Button btn_Ok;

	@FXML
	private TextArea txt_error;

	public void init(String error, String exception) {
		lbl_errorMsg.setText(error);
		txt_error.setText(exception);
	}

	/* function performed when the ok button clicked. Close the message */
	public void okButtonClicked(ActionEvent event) {
		Stage stage = (Stage) btn_Ok.getScene().getWindow();
		stage.close();
	}

}

package com.techneo;

import java.util.Random;

/*
 * 
 * author: Gayathree Munasinghe
 * class name: CandidateAssignment
 * action: maps students and projects 
 * 
 */
public class CandidateAssignment {

	private String project;

	private StudentEntry student;

	private String previousProject;

	public CandidateAssignment() {
	}

	/*
	 * construct a candidate assignment object using studententry object
	 */
	public CandidateAssignment(StudentEntry student) {
		super();
		this.student = student;
		this.randomizeAssignment();
	}

	public String getProject() {
		return project;
	}

	public void setProject(String project) {
		this.project = project;
	}

	public StudentEntry getStudent() {
		return student;
	}

	public void setStudent(StudentEntry student) {
		this.student = student;
	}

	/* assign a random project to the student */
	public void randomizeAssignment() {
		Random random = new Random();
		int projectNumber = 0;
		if (!this.student.isHasPreAssignedProject()) {
			projectNumber = random.nextInt(10);
		}
		this.previousProject = this.project;
		this.project = this.student.getOrderedPreferences().get(projectNumber);
	}

	/*
	 * undo the most recent random assignment
	 */
	public void undoChange() {
		this.project = this.previousProject;
	}

	StudentEntry getStudentEntry() {
		return this.student;
	}

	String getAssignedProject() {
		return this.project;
	}

	/* determine the energy of the assignment */
	public int getEnergy() {
		if (this.student.getRanking(this.project) != -1)
			return (this.student.getRanking(this.project)) * (this.student.getRanking(this.project));
		else {
			return 100;
		}
	}

	@Override
	public String toString() {
		return this.student.getStudentName() + " : " + this.project;
	}
}

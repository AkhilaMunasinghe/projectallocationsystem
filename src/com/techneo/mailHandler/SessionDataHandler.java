package com.techneo.mailHandler;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

import com.techneo.ui.ErrorMessageUIController;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/*
 * 
 * author: Gayathree Munasinghe
 * class name: SessionDataHandler
 * action: handle the sessions
 * 
 */
public class SessionDataHandler {

	private final String SESSION_FILE_PATH = "resources" + File.separator + "files" + File.separator
			+ "sessionDataFile.csv";

	private static final String COMMA = ",";

	private static SessionDataHandler instance;

	private List<SessionData> sessionDataList = new ArrayList<>();

	public List<SessionData> getSessionDataList() {
		return sessionDataList;
	}

	public void setSessionDataList(List<SessionData> sessionDataList) {
		this.sessionDataList = sessionDataList;
	}

	public static SessionDataHandler getInstance() throws IOException {
		if (instance == null) {
			instance = new SessionDataHandler();
		}
		return instance;
	}

	public static void setInstance(SessionDataHandler instance) {
		SessionDataHandler.instance = instance;
	}

	private SessionDataHandler() throws IOException {
		this.getSessionsData();
	}

	/*
	 * read the session data file in the start and load all the session data
	 * available
	 */
	public void getSessionsData() throws IOException {
		FileInputStream fileInputStream = null;
		InputStreamReader inputStreamreader = null;
		BufferedReader bufferedReader = null;
		try {
			fileInputStream = new FileInputStream(this.SESSION_FILE_PATH);
			inputStreamreader = new InputStreamReader(fileInputStream);
			bufferedReader = new BufferedReader(inputStreamreader);
			String line = bufferedReader.readLine();
			while (line != null) {
				line = bufferedReader.readLine();
				if (line != null && !line.equalsIgnoreCase("")) {
					String[] data = line.split(COMMA);
					if (data.length != 0) {
						SessionData sessionData = new SessionData(data[0], data[1], data[2], data[3], data[4], data[5],
								data[6], data[7], data[8], data[9]);
						this.sessionDataList.add(sessionData);
					}
				}
			}
		} catch (Exception ex) {
			FXMLLoader errorloader = new FXMLLoader(getClass().getResource("ErrorMessageUIFxml.fxml"));
			Stage errorstage = new Stage(StageStyle.DECORATED);
			errorstage.setResizable(false);
			errorstage.setTitle("Project Allocator-Error");
			errorstage.setScene(new Scene((Pane) errorloader.load()));
			ErrorMessageUIController errorCntroller = errorloader.<ErrorMessageUIController> getController();
			errorCntroller.init("Error while reading session data file!", "Actual error: \n" + ex.toString());
			errorstage.show();
		} finally {
			try {
				fileInputStream.close();
				bufferedReader.close();
				inputStreamreader.close();
			} catch (Exception ex) {
				FXMLLoader errorloader = new FXMLLoader(getClass().getResource("ErrorMessageUIFxml.fxml"));
				Stage errorstage = new Stage(StageStyle.DECORATED);
				errorstage.setResizable(false);
				errorstage.setTitle("Project Allocator-Error");
				errorstage.setScene(new Scene((Pane) errorloader.load()));
				ErrorMessageUIController errorCntroller = errorloader.<ErrorMessageUIController> getController();
				errorCntroller.init("Error while reading session data file!", "Actual error: \n" + ex.toString());
				errorstage.show();
			}
		}
	}

	/* add new session data */
	public void addSessionData(SessionData sessionData) throws IOException {
		this.sessionDataList.add(sessionData);
		writeSessionDataFile();
	}

	/* write session data to the file */
	public void writeSessionDataFile() throws IOException {
		FileOutputStream outputStream = null;
		BufferedWriter bufferedWriter = null;
		OutputStreamWriter outputStreamWriter = null;
		try {
			outputStream = new FileOutputStream(this.SESSION_FILE_PATH, false);
			outputStreamWriter = new OutputStreamWriter(outputStream);
			bufferedWriter = new BufferedWriter(outputStreamWriter);
			String titleLine = "BatchNumber" + COMMA + "Folder" + COMMA + "MailTitle" + COMMA + "MailAddress" + COMMA
					+ "Password" + COMMA + "Deadline" + COMMA + "Status" + COMMA + "mailAddressFile" + COMMA
					+ "dataFile" + COMMA + "ProjectListFile";
			bufferedWriter.write(titleLine);
			for (SessionData sessionData : sessionDataList) {
				bufferedWriter.newLine();
				String line = sessionData.getBatch() + COMMA + sessionData.getMailFolder() + COMMA
						+ sessionData.getMailTitle() + COMMA + sessionData.getMailAddress() + COMMA
						+ sessionData.getMailPassWord() + COMMA + sessionData.getDeadline() + COMMA
						+ sessionData.getStatus() + COMMA + sessionData.getMailAddressFile() + COMMA
						+ sessionData.getDataFile() + COMMA + sessionData.getProjectListFile();
				bufferedWriter.write(line);
			}
			bufferedWriter.flush();
		} catch (Exception ex) {
			FXMLLoader errorloader = new FXMLLoader(getClass().getResource("ErrorMessageUIFxml.fxml"));
			Stage errorstage = new Stage(StageStyle.DECORATED);
			errorstage.setResizable(false);
			errorstage.setTitle("Project Allocator-Error");
			errorstage.setScene(new Scene((Pane) errorloader.load()));
			ErrorMessageUIController errorCntroller = errorloader.<ErrorMessageUIController> getController();
			errorCntroller.init("Error while writting sesson data file!", "Actual error: \n" + ex.toString());
			errorstage.show();
		} finally {
			try {
				outputStream.close();
				bufferedWriter.close();
				outputStreamWriter.close();
			} catch (Exception ex) {
				FXMLLoader errorloader = new FXMLLoader(getClass().getResource("ErrorMessageUIFxml.fxml"));
				Stage errorstage = new Stage(StageStyle.DECORATED);
				errorstage.setResizable(false);
				errorstage.setTitle("Project Allocator-Error");
				errorstage.setScene(new Scene((Pane) errorloader.load()));
				ErrorMessageUIController errorCntroller = errorloader.<ErrorMessageUIController> getController();
				errorCntroller.init("Error while writting session data file!", "Actual error: \n" + ex.toString());
				errorstage.show();
			}
		}
	}

	/* remove the session data from the file */
	public void removeSession(int sessionDataIndex) throws IOException {
		this.sessionDataList.remove(sessionDataIndex);
		this.writeSessionDataFile();
	}

	public void editSession(int sessionDataIndex, SessionData newSessionData) throws IOException {
		this.sessionDataList.set(sessionDataIndex, newSessionData);
		this.writeSessionDataFile();
	}

}

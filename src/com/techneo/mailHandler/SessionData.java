package com.techneo.mailHandler;

/*
 * 
 * author: Gayathree Munasinghe
 * class name: SessionData
 * action: store data about the sessions that have been entered to the system
 * 
 */

public class SessionData {

	/**
	 * 
	 */
	public SessionData() {
		super();
	}

	private String batch;

	private String mailFolder;

	private String mailTitle;

	private String mailAddress;

	private String mailPassWord;

	private String deadline;

	private String status;

	private String mailAddressFile;

	private String dataFile;

	private String ProjectListFile;

	public String getBatch() {
		return batch;
	}

	public void setBatch(String batch) {
		this.batch = batch;
	}

	public String getMailFolder() {
		return mailFolder;
	}

	public void setMailFolder(String mailFolder) {
		this.mailFolder = mailFolder;
	}

	public String getMailAddress() {
		return mailAddress;
	}

	public void setMailAddress(String mailAddress) {
		this.mailAddress = mailAddress;
	}

	public String getMailPassWord() {
		return mailPassWord;
	}

	public void setMailPassWord(String mailPassWord) {
		this.mailPassWord = mailPassWord;
	}

	public String getDeadline() {
		return deadline;
	}

	public void setDeadline(String deadline) {
		this.deadline = deadline;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMailAddressFile() {
		return mailAddressFile;
	}

	public void setMailAddressFile(String mailAddressFile) {
		this.mailAddressFile = mailAddressFile;
	}

	public String getDataFile() {
		return dataFile;
	}

	public void setDataFile(String dataFile) {
		this.dataFile = dataFile;
	}

	public String getMailTitle() {
		return mailTitle;
	}

	public void setMailTitle(String mailTitle) {
		this.mailTitle = mailTitle;
	}

	public String getProjectListFile() {
		return ProjectListFile;
	}

	public void setProjectListFile(String projectListFile) {
		ProjectListFile = projectListFile;
	}

	/**
	 * @param batch
	 * @param mailFolder
	 * @param mailAddress
	 * @param mailPassWord
	 * @param deadline
	 * @param status
	 * @param mailAddressFile
	 * @param dataFile
	 * @param projectListFile
	 */
	public SessionData(String batch, String mailFolder, String mailTitle, String mailAddress, String mailPassWord,
			String deadline, String status, String mailAddressFile, String dataFile, String projectListFile) {
		super();
		this.batch = batch;
		this.mailFolder = mailFolder;
		this.mailTitle = mailTitle;
		this.mailAddress = mailAddress;
		this.mailPassWord = mailPassWord;
		this.deadline = deadline;
		this.status = status;
		this.mailAddressFile = mailAddressFile;
		this.dataFile = dataFile;
		this.ProjectListFile = projectListFile;
	}

}

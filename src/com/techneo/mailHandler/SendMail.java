package com.techneo.mailHandler;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import java.util.Map.Entry;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.*;

import com.techneo.CandidateAssignment;
import com.techneo.CandidateSolution;
import com.techneo.ui.ErrorMessageUIController;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/*
 * 
 * author: Chamaka Wimalarathne, Gayathree Munasinghe
 * class name: SendMail
 * action: send e-mails to student
 * 
 */

public class SendMail {
	private MimeMessage mail;

	/**
	 * @param senderAddress
	 * @param password
	 */
	public SendMail(String senderAddress, String password) {
		super();
		this.senderAddress = senderAddress;
		this.password = password;
		Authenticator auth = new SMTPAuthenticator();
		Session session = Session.getInstance(this.setProperties(), auth);
		this.mail = new MimeMessage(session);
	}

	private String senderAddress;

	private String password;

	private static final String COMMA = ",";

	public String getSenderAddress() {
		return senderAddress;
	}

	public void setSenderAddress(String senderAddress) {
		this.senderAddress = senderAddress;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public class SMTPAuthenticator extends javax.mail.Authenticator {
		@Override
		public PasswordAuthentication getPasswordAuthentication() {
			return new PasswordAuthentication(senderAddress, password);
		}
	}

	/* function to set properties of the mail session */
	private Properties setProperties() {
		Properties properties = new Properties();
		properties.put("mail.smtp.user", this.senderAddress);
		properties.put("mail.smtp.host", "smtp.gmail.com");
		properties.put("mail.smtp.port", "465");
		properties.put("mail.smtp.starttls.enable", "true");
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.smtp.socketFactory.port", "465");
		properties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		properties.put("mail.smtp.socketFactory.fallback", "false");
		return properties;
	}

	/* function to send project request mail with two attachments */
	public void sendProjectReqestMail(SessionData sessionData) throws MessagingException, IOException {
		Multipart multipart = new MimeMultipart();
		BodyPart messageBodyPartProjectList = new MimeBodyPart();
		messageBodyPartProjectList = new MimeBodyPart();
		DataSource projectListData = new FileDataSource(sessionData.getProjectListFile());
		messageBodyPartProjectList.setDataHandler(new DataHandler(projectListData));
		messageBodyPartProjectList.setFileName(sessionData.getProjectListFile());
		multipart.addBodyPart(messageBodyPartProjectList);
		BodyPart messageBodyPartForm = new MimeBodyPart();
		String filename = "resources" + File.separator + "files" + File.separator + "ProjectDataForm.xlsx";
		DataSource source2 = new FileDataSource(filename);
		messageBodyPartForm.setDataHandler(new DataHandler(source2));
		messageBodyPartForm.setFileName(filename);
		multipart.addBodyPart(messageBodyPartForm);
		Map<String, String> mailAddressMap = this.readMailAdressFile(sessionData.getMailAddressFile());
		Iterator<Entry<String, String>> iterator = mailAddressMap.entrySet().iterator();
		BodyPart messageText = new MimeBodyPart();
		String message = "";
		multipart.addBodyPart(messageText);
		while (iterator.hasNext()) {
			Entry<String, String> currentEntry = iterator.next();
			message = "Hello " + currentEntry.getKey()
					+ ", \n \nAttached documents are a list of projects and an excel form to be filled by you. Please fill the form accordingly and send to "
					+ sessionData.getMailAddress() + " on or before " + sessionData.getDeadline()
					+ " with subject as \"" + sessionData.getMailTitle() + "\". \n \nThankyou";
			messageText.setText(message);
			this.sendMailToStudent(currentEntry.getValue(),
					"Preferences for the final project-" + sessionData.getBatch(), multipart);
		}
	}

	/*
	 * function to read the mail address file and return a map of student name
	 * and mail address
	 */
	private Map<String, String> readMailAdressFile(String filePath) throws IOException {
		Map<String, String> studentMailMap = new HashMap<>();
		FileInputStream fileInputStream = null;
		InputStreamReader inputStreamreader = null;
		BufferedReader bufferedReader = null;
		try {
			fileInputStream = new FileInputStream(filePath);
			inputStreamreader = new InputStreamReader(fileInputStream);
			bufferedReader = new BufferedReader(inputStreamreader);
			String line = bufferedReader.readLine();
			while (line != null) {
				line = bufferedReader.readLine();
				if (line != null) {
					String[] data = line.split(COMMA);
					studentMailMap.put(data[0], data[1]);
				}
			}
		} catch (Exception ex) {
			FXMLLoader errorloader = new FXMLLoader(getClass().getResource("ErrorMessageUIFxml.fxml"));
			Stage errorstage = new Stage(StageStyle.DECORATED);
			errorstage.setResizable(false);
			errorstage.setTitle("Project Allocator-Error");
			errorstage.setScene(new Scene((Pane) errorloader.load()));
			ErrorMessageUIController errorCntroller = errorloader.<ErrorMessageUIController> getController();
			errorCntroller.init("Error while reading mail list!", "Actual error: \n" + ex.toString());
			errorstage.show();
		} finally {
			try {
				fileInputStream.close();
				bufferedReader.close();
				inputStreamreader.close();
			} catch (Exception ex) {
				FXMLLoader errorloader = new FXMLLoader(getClass().getResource("ErrorMessageUIFxml.fxml"));
				Stage errorstage = new Stage(StageStyle.DECORATED);
				errorstage.setResizable(false);
				errorstage.setTitle("Project Allocator-Error");
				errorstage.setScene(new Scene((Pane) errorloader.load()));
				ErrorMessageUIController errorCntroller = errorloader.<ErrorMessageUIController> getController();
				errorCntroller.init("Error while reading mail list!", "Actual error: \n" + ex.toString());
				errorstage.show();
			}
		}
		return studentMailMap;
	}

	/* function to send email to the student */
	private void sendMailToStudent(String reciever, String subject, Multipart messageContent)
			throws MessagingException {
		mail.setSubject(subject);
		mail.setContent(messageContent);
		mail.setFrom(new InternetAddress(this.senderAddress));
		mail.addRecipient(Message.RecipientType.TO, new InternetAddress(reciever));
		Transport.send(mail);
	}

	/* function to send results to students */
	public void mailResults(String filePath, CandidateSolution solution) throws Exception {
		Map<String, String> mailAddressMap = this.readMailAdressFile(filePath);
		Multipart multipart = new MimeMultipart();
		BodyPart messageText = new MimeBodyPart();
		String message = "";
		multipart.addBodyPart(messageText);
		for (CandidateAssignment assignment : solution.getCandidateAssignmentList()) {
			if (mailAddressMap.containsKey(assignment.getStudent().getStudentName())) {
				message = "Hi " + assignment.getStudent().getStudentName()
						+ ", \n \n You have been allocated with the project \"" + assignment.getProject()
						+ "\". \n \n Good luck";
				messageText.setText(message);
				this.sendMailToStudent(mailAddressMap.get(assignment.getStudent().getStudentName()),
						"Project Allocation Results", multipart);
				System.out.println("Mail sent to " + assignment.getStudent().getStudentName());
			} else {
				throw new Exception(
						"E-mail address of student " + assignment.getStudent().getStudentName() + " is not available");
				// System.out.println("Mail address of the student does not
				// exist");
				// FXMLLoader errorloader = new
				// FXMLLoader(getClass().getResource("ErrorMessageUIFxml.fxml"));
				// Stage errorstage = new Stage(StageStyle.DECORATED);
				// errorstage.setResizable(false);
				// errorstage.setTitle("Project Allocator-Error");
				// errorstage.setScene(new Scene((Pane) errorloader.load()));
				// ErrorMessageUIController errorCntroller =
				// errorloader.<ErrorMessageUIController> getController();
				// errorCntroller.init("Error while e-mailing results!", "Actual
				// error: \n E-mal address for student "
				// + assignment.getStudent().getStudentName() + " not found.");
				// errorstage.show();
			}
		}
	}

}

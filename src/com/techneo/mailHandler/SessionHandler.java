package com.techneo.mailHandler;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;

import javax.mail.BodyPart;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.search.AndTerm;
import javax.mail.search.FlagTerm;
import javax.mail.search.SearchTerm;
import javax.mail.search.SubjectTerm;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.techneo.CandidateSolution;
import com.techneo.ui.ErrorMessageUIController;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/*
 * 
 * author: Gayathree Munasinghe
 * class name: SessionHandler
 * action: handle all executable sessions
 * 
 */
public class SessionHandler {

	private static SessionHandler instance = null;

	private static final String TAB = "\t";

	public static SessionHandler getInstance() {
		if (instance == null) {
			instance = new SessionHandler();
		}
		return instance;
	}

	public static void setInstance(SessionHandler instance) {
		SessionHandler.instance = instance;
	}

	/* start all new and pending sessions in startup */
	public void startSessions() throws Exception {
		for (int i = 0; i < SessionDataHandler.getInstance().getSessionDataList().size(); i++) {
			SessionData sessionData = SessionDataHandler.getInstance().getSessionDataList().get(i);
			if (sessionData.getStatus().equalsIgnoreCase("new")) {
				this.getMail(sessionData);
			}
			DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Date deadLine = dateFormat.parse(sessionData.getDeadline() + " 23:59:59");
			if (Calendar.getInstance().getTime().after(deadLine)) {
				sessionData.setStatus("completed");
				SessionDataHandler.getInstance().editSession(i, sessionData);
			}
		}
	}

	/* start new sessions and update status */

	/* end the session and update status */

	private void getMail(SessionData sessionData) throws Exception {
		Properties properties = new Properties();
		properties.put("mail.store.protocol", "imaps");
		properties.put("mail.imaps.host", "imap.gmail.com");
		properties.put("mail.imaps.port", "993");
		properties.put("mail.imaps.timeout", "10000");
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date deadLine = dateFormat.parse(sessionData.getDeadline() + " 23:59:59");

		Session emailSession = Session.getDefaultInstance(properties);

		// create the POP3 store object and connect with the pop server
		Store store = emailSession.getStore("imaps");

		store.connect(sessionData.getMailAddress(), sessionData.getMailPassWord());

		// create the folder object and open it
		Folder emailFolder = store.getFolder(sessionData.getMailFolder());
		emailFolder.open(Folder.READ_WRITE);

		FlagTerm unseenSarchTerm = new FlagTerm(new Flags(Flags.Flag.SEEN), false);
		SubjectTerm subjectSearchTerm = new SubjectTerm(sessionData.getMailTitle());
		SearchTerm[] searchTerms = { unseenSarchTerm, subjectSearchTerm };
		// retrieve the messages from the folder in an array and print it
		AndTerm search = new AndTerm(searchTerms);
		Message[] messages = emailFolder.search(search);
		System.out.println("messages.length---" + messages.length + "----from----" + sessionData.getMailAddress());
		for (int i = 0, n = messages.length; i < n; i++) {
			Message message = messages[i];
			message.setFlag(Flags.Flag.SEEN, true);
			if (message.getSentDate().compareTo(deadLine) <= 0) {
				Multipart messageContentMultiPart = (Multipart) message.getContent();
				for (int j = 0; j < messageContentMultiPart.getCount(); j++) {
					if (messageContentMultiPart.getBodyPart(j).getDisposition() != null
							&& messageContentMultiPart.getBodyPart(j).getDisposition().equalsIgnoreCase(Part.ATTACHMENT)
							&& messageContentMultiPart.getBodyPart(j).getFileName().contains(".xlsx")) {
						this.downloadAttachment(messageContentMultiPart.getBodyPart(j), sessionData,
								message.getFrom()[0].toString());
					}
				}
			}
		}
		// close the store and folder objects
		emailFolder.close(false);
		store.close();

	}

	/* function to download attachments sent by students */
	private void downloadAttachment(BodyPart content, SessionData sessionData, String studentName) throws IOException {
		studentName = studentName.substring(0, studentName.indexOf("<")).replace(".", "_");
		InputStream inputStream = null;
		File file = null;
		FileOutputStream fos = null;
		try {
			file = new File(sessionData.getDataFile() + File.separator + studentName + "_" + content.getFileName());
			fos = new FileOutputStream(file);
			inputStream = content.getInputStream();
			byte[] buf = new byte[4096];
			int bytesRead;
			while ((bytesRead = inputStream.read(buf)) != -1) {
				fos.write(buf, 0, bytesRead);
			}
			this.storeProjectData(file, sessionData);
		} catch (Exception ex) {
			FXMLLoader errorloader = new FXMLLoader(getClass().getResource("ErrorMessageUIFxml.fxml"));
			Stage errorstage = new Stage(StageStyle.DECORATED);
			errorstage.setResizable(false);
			errorstage.setTitle("Project Allocator-Error");
			errorstage.setScene(new Scene((Pane) errorloader.load()));
			ErrorMessageUIController errorCntroller = errorloader.<ErrorMessageUIController> getController();
			errorCntroller.init("Error while downloading e-mail attachment!", "Actual error: \n" + ex.toString());
			errorstage.show();
		} finally {
			try {
				inputStream.close();
				fos.close();
			} catch (Exception ex) {
				FXMLLoader errorloader = new FXMLLoader(getClass().getResource("ErrorMessageUIFxml.fxml"));
				Stage errorstage = new Stage(StageStyle.DECORATED);
				errorstage.setResizable(false);
				errorstage.setTitle("Project Allocator-Error");
				errorstage.setScene(new Scene((Pane) errorloader.load()));
				ErrorMessageUIController errorCntroller = errorloader.<ErrorMessageUIController> getController();
				errorCntroller.init("Error while downloading mail attachments!", "Actual error: \n" + ex.toString());
				errorstage.show();
			}
		}
	}

	/*
	 * function to get data in the attachments sent by students and store it in
	 * a .tsv file
	 */
	private void storeProjectData(File dataFile, SessionData sessionData) throws IOException {
		// read data from attachment
		XSSFWorkbook book = null;
		File tsvFile = null;
		FileOutputStream outputStream = null;
		BufferedWriter bufferedWriter = null;
		OutputStreamWriter outputStreamWriter = null;
		try {
			book = new XSSFWorkbook(dataFile);
			Sheet sheet = book.getSheetAt(0);
			String line = "";
			for (int i = 3; i < 15; i++) {
				if (sheet.getRow(i).getCell(3) == null) {
					break;

				}
				line = line + sheet.getRow(i).getCell(3).getStringCellValue() + TAB;
			}
			// add data to project allocation data file
			line.substring(0, line.lastIndexOf(TAB) + 1);
			tsvFile = new File(
					sessionData.getDataFile() + File.separator + sessionData.getBatch() + "_ProjectAllocationData.tsv");
			if (!tsvFile.exists()) {
				outputStream = new FileOutputStream(tsvFile, false);
				outputStreamWriter = new OutputStreamWriter(outputStream);
				bufferedWriter = new BufferedWriter(outputStreamWriter);
				String titleLine = "Student Name" + TAB + "Prearranged" + TAB + "Preference 1" + TAB + "Preference 2"
						+ TAB + "Preference 3" + TAB + "Preference 4" + TAB + "Preference 5" + TAB + "Preference 6"
						+ TAB + "Preference 7" + TAB + "Preference 8" + TAB + "Preference 9" + TAB + "Preference 10";
				bufferedWriter.write(titleLine);
				bufferedWriter.newLine();
				bufferedWriter.write(line);
			} else {
				outputStream = new FileOutputStream(tsvFile, true);
				outputStreamWriter = new OutputStreamWriter(outputStream);
				bufferedWriter = new BufferedWriter(outputStreamWriter);
				bufferedWriter.newLine();
				bufferedWriter.write(line);
			}
			bufferedWriter.flush();
		} catch (Exception ex) {
			FXMLLoader errorloader = new FXMLLoader(getClass().getResource("ErrorMessageUIFxml.fxml"));
			Stage errorstage = new Stage(StageStyle.DECORATED);
			errorstage.setResizable(false);
			errorstage.setTitle("Project Allocator-Error");
			errorstage.setScene(new Scene((Pane) errorloader.load()));
			ErrorMessageUIController errorCntroller = errorloader.<ErrorMessageUIController> getController();
			errorCntroller.init("Error while reading e-mail attachments!", "Actual error: \n" + ex.toString());
			errorstage.show();
		} finally {
			try {
				book.close();
				outputStream.close();
				bufferedWriter.close();
				outputStreamWriter.close();
			} catch (IOException ex) {
				FXMLLoader errorloader = new FXMLLoader(getClass().getResource("ErrorMessageUIFxml.fxml"));
				Stage errorstage = new Stage(StageStyle.DECORATED);
				errorstage.setResizable(false);
				errorstage.setTitle("Project Allocator-Error");
				errorstage.setScene(new Scene((Pane) errorloader.load()));
				ErrorMessageUIController errorCntroller = errorloader.<ErrorMessageUIController> getController();
				errorCntroller.init("Error while reading e-mail attachments!", "Actual error: \n" + ex.toString());
				errorstage.show();
			}
		}
	}
}

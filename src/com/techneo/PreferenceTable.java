package com.techneo;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

import com.techneo.ui.ErrorMessageUIController;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.util.Random;
import java.util.StringTokenizer;
import java.util.Vector;

/*
 * 
 * author: Gayathree Munasinghe
 * class name: PreferenceTable
 * action: store data from the "Project allocation data.tsv" file
 * 
 */

public class PreferenceTable {

	private static final String TAB = "\t";

	private List<StudentEntry> studentEntryList = new ArrayList<>();

	private Hashtable<String, StudentEntry> studentEntryTable = new Hashtable<>();

	Vector<Vector<String>> dataVector;

	private Random RND = new Random();

	public PreferenceTable() {
	}

	public PreferenceTable(String fileName) throws IOException {
		dataVector = this.loadContentFromFile(fileName);
		for (Vector<String> studentData : dataVector) {
			StudentEntry studentEntry = new StudentEntry();
			studentEntry.setStudentName(studentData.get(0));
			if (studentData.get(1).equalsIgnoreCase("no") || studentData.get(1).equalsIgnoreCase("false")) {
				studentEntry.setHasPreAssignedProject(false);
			} else if (studentData.get(1).equalsIgnoreCase("yes") || studentData.get(1).equalsIgnoreCase("true")) {
				studentEntry.setHasPreAssignedProject(true);
			}
			studentEntry.setOrderedPreferences(studentData.subList(2, studentData.size()));
			studentEntryList.add(studentEntry);
			studentEntryTable.put(studentEntry.getStudentName(), studentEntry);
		}
		this.fillPreferencesOfAll(10);
	}

	/* read the file and return the content as string vector */
	private Vector<Vector<String>> loadContentFromFile(String fileName) throws IOException {
		FileInputStream fileInputStream = null;
		InputStreamReader inputStreamreader = null;
		BufferedReader bufferedReader = null;
		StringTokenizer tokernizer = null;
		Vector<Vector<String>> lineVector = null;
		try {
			fileInputStream = new FileInputStream(fileName);
			inputStreamreader = new InputStreamReader(fileInputStream);
			bufferedReader = new BufferedReader(inputStreamreader);
			lineVector = new Vector<>();
			String line = bufferedReader.readLine();
			while (line != null) {
				line = bufferedReader.readLine();
				if (line != null) {
					Vector<String> tokenVector = new Vector<>();
					tokernizer = new StringTokenizer(line, TAB);
					while (tokernizer.hasMoreTokens()) {
						String token = tokernizer.nextToken();
						tokenVector.addElement(token.intern());
					}
					lineVector.addElement(tokenVector);
				}
			}
		} catch (Exception ex) {
			FXMLLoader errorloader = new FXMLLoader(getClass().getResource("ErrorMessageUIFxml.fxml"));
			Stage errorstage = new Stage(StageStyle.DECORATED);
			errorstage.setResizable(false);
			errorstage.setTitle("Project Allocator-Error");
			errorstage.setScene(new Scene((Pane) errorloader.load()));
			ErrorMessageUIController errorCntroller = errorloader.<ErrorMessageUIController> getController();
			errorCntroller.init("Error while reading data file!", "Actual error: \n" + ex.toString());
			errorstage.show();
		} finally {
			try {
				fileInputStream.close();
				bufferedReader.close();
				inputStreamreader.close();
			} catch (Exception ex) {
				FXMLLoader errorloader = new FXMLLoader(getClass().getResource("ErrorMessageUIFxml.fxml"));
				Stage errorstage = new Stage(StageStyle.DECORATED);
				errorstage.setResizable(false);
				errorstage.setTitle("Project Allocator-Error");
				errorstage.setScene(new Scene((Pane) errorloader.load()));
				ErrorMessageUIController errorCntroller = errorloader.<ErrorMessageUIController> getController();
				errorCntroller.init("Error while reading data file!", "Actual error: \n" + ex.toString());
				errorstage.show();
			}
		}
		return lineVector;
	}

	/*
	 * returns a list of instances of the class StudentEntry, each of which
	 * corresponds to a different student in the preference table
	 */
	public List<StudentEntry> getAllStudentEntries() {
		return this.studentEntryList;
	}

	/*
	 * returns the instance of StudentEntry that corresponds to student named
	 * sname. Return null if there is no entry for this name
	 */
	public StudentEntry getEntryFor(String sname) {
		return this.studentEntryTable.get(sname);
	}

	/* generate and return a random student */
	public StudentEntry getRandomStudent() {
		int randomStudentNumber = RND.nextInt(this.studentEntryTable.size());
		return this.getAllStudentEntries().get(randomStudentNumber);
	}

	/* generate and return a random project preference */
	public String getRandomPreference() {
		return this.getRandomStudent().getRandomPreference();
	}

	/*
	 * add extra random projects to every StudentEntry in the table until it has
	 * maxPrefs projects
	 */
	public void fillPreferencesOfAll(int maxPrefs) {
		for (StudentEntry student : this.studentEntryList) {
			if (!student.isHasPreAssignedProject()) {
				int numberOfPreferences = student.getNumberOfStatedPreferences();
				while (numberOfPreferences < maxPrefs) {
					String randomProject = this.getRandomPreference();
					while (student.hasPreference(randomProject.intern())) {
						randomProject = this.getRandomPreference();
					}
					student.addProject(randomProject);
					numberOfPreferences++;
				}
			}
		}

	}

}

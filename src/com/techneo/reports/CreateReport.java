package com.techneo.reports;

import java.io.File;
import java.io.IOException;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;
import javax.imageio.ImageIO;

import com.techneo.ui.ErrorMessageUIController;

import javafx.application.Application;
import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/*
 * 
 * author: Chamaka Wimalarathne
 * class name: CreateReport
 * action: generate the reports
 * 
 */
public class CreateReport {

	// a chart should pop up after you have generated the outputs
	// this is the easiest way to get compare two algorithms and get the best
	// one
	// ------------------------------------------------
	final static String preference1 = "First";
	final static String preference2 = "Second";
	final static String preference3 = "Third";
	final static String preference4 = "Fourth";
	final static String preference5 = "Fifth";
	final static String preference6 = "Sixth";
	final static String preference7 = "Seventh";
	final static String preference8 = "Eighth";
	final static String preference9 = "Nineth";
	final static String preference10 = "Tenth";
	final static String preferencenon = "Non Preferred";
	static float pref1 = 0;
	static float pref2 = 0;
	static float pref3 = 0;
	static float pref4 = 0;
	static float pref5 = 0;
	static float pref6 = 0;
	static float pref7 = 0;
	static float pref8 = 0;
	static float pref9 = 0;
	static float pref10 = 0;
	static float prefnon = 0;
	// generic algorithm data
	static float pref11 = 0;
	static float pref21 = 0;
	static float pref31 = 0;
	static float pref41 = 0;
	static float pref51 = 0;
	static float pref61 = 0;
	static float pref71 = 0;
	static float pref81 = 0;
	static float pref91 = 0;
	static float pref101 = 0;
	static float prefnon1 = 0;
	private int n = 0;
	private String filepath = null; // set the excel files file path here also
	// you can set it by passing to drawChart(String path) method
	// ------------------------------------------------

	// ----------------values_to_draw_graph----------------------

	public void GraphDataEnter(int a, List<Integer> table) {
		RestBars();
		n = a;
		for (int i = 0; i < table.size(); i++) {
			setValue_SA(table.get(i));
		}
		try {
			starting();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// --------------------------save_chart_to_a_image---------------------

	public void GraphDataEnterSave(int a, List<Integer> table, String path) throws IOException {
		RestBars();
		n = a;
		filepath = path;

		for (int i = 0; i < table.size(); i++) {
			setValue_SA(table.get(i));
		}
		try {
			starting();
		} catch (Exception ex) {
			FXMLLoader errorloader = new FXMLLoader(getClass().getResource("ErrorMessageUIFxml.fxml"));
			Stage errorstage = new Stage(StageStyle.DECORATED);
			errorstage.setResizable(false);
			errorstage.setTitle("Project Allocator-Error");
			errorstage.setScene(new Scene((Pane) errorloader.load()));
			ErrorMessageUIController errorCntroller = errorloader.<ErrorMessageUIController> getController();
			errorCntroller.init("Error while generating reports!", "Actual error: \n" + ex.toString());
			errorstage.show();
		}
		// DrawaBars();
	}
	// ----------------------------------------------------------

	// -------------simulated_annealing_data------------------
	public void setValue_SA(int pref) {
		switch (pref) {
		case 1:
			pref1 = pref1 + 1;
			break;
		case 2:
			pref2 = pref2 + 1;
			break;
		case 3:
			pref3 = pref3 + 1;
			break;
		case 4:
			pref4 = pref4 + 1;
			break;
		case 5:
			pref5 = pref5 + 1;
			break;
		case 6:
			pref6 = pref6 + 1;
			break;
		case 7:
			pref7 = pref7 + 1;
			break;
		case 8:
			pref8 = pref8 + 1;
			break;
		case 9:
			pref9 = pref9 + 1;
			break;
		case 10:
			pref10 = pref10 + 1;
			break;
		default:
			prefnon = prefnon + 1;
			break;
		}
	}
	// ---------------------------------------------------------------

	// -------------Generic_algorithm_data------------------
	// call this methord and pass it the value of the preference in the main
	// class
	public void setValue_GA(int pref) {
		switch (pref) {
		case 1:
			pref11 = pref11 + 1;
			break;
		case 2:
			pref21 = pref21 + 1;
			break;
		case 3:
			pref31 = pref31 + 1;
			break;
		case 4:
			pref41 = pref41 + 1;
			break;
		case 5:
			pref51 = pref51 + 1;
			break;
		case 6:
			pref61 = pref61 + 1;
			break;
		case 7:
			pref71 = pref71 + 1;
			break;
		case 8:
			pref81 = pref81 + 1;
			break;
		case 9:
			pref91 = pref91 + 1;
			break;
		case 10:
			pref101 = pref101 + 1;
			break;
		default:
			prefnon1 = prefnon1 + 1;
			break;
		}
	}
	// ---------------------------------------------------------------

	// ----------------start_drawing-------------------------------
	// first vector has simulated annealings results , second one generic
	// algorithm
	// public static void DrawaBars(){
	// String[] stage = null;
	// launch(stage);
	// }
	// ---------------------------------------------------
	//
	// //-----------Reset_bar_values---------
	// // Call this methord when recalculating the values
	// // object the values will be resetted
	public void RestBars() {
		pref1 = 0;
		pref2 = 0;
		pref3 = 0;
		pref4 = 0;
		pref5 = 0;
		pref6 = 0;
		pref7 = 0;
		pref8 = 0;
		pref9 = 0;
		pref10 = 0;
		prefnon = 0;
		// generic algorithm data
		pref11 = 0;
		pref21 = 0;
		pref31 = 0;
		pref41 = 0;
		pref51 = 0;
		pref61 = 0;
		pref71 = 0;
		pref81 = 0;
		pref91 = 0;
		pref101 = 0;
		prefnon1 = 0;
	}

	// @Override
	// public void start(Stage stage) throws Exception {
	public void starting() throws Exception {
		Stage stage = new Stage();
		// TODO Auto-generated method stub
		stage.setTitle("Project Allocation-Reports");
		final CategoryAxis xAxis = new CategoryAxis();
		final NumberAxis yAxis = new NumberAxis();
		final BarChart<String, Number> bc = new BarChart<String, Number>(xAxis, yAxis);
		bc.setTitle("Project Summary");
		xAxis.setLabel("Preferences");
		yAxis.setLabel("Value");
		XYChart.Series series1 = new XYChart.Series();

		if (n == 1) {
			series1.setName("Generic Algorithm Allocation Summery");
		} else {
			series1.setName("simulated annealing Algorithm Allocation Summery");
		}

		series1.getData().add(new XYChart.Data(preference1, pref1));
		series1.getData().add(new XYChart.Data(preference2, pref2));
		series1.getData().add(new XYChart.Data(preference3, pref3));
		series1.getData().add(new XYChart.Data(preference4, pref4));
		series1.getData().add(new XYChart.Data(preference5, pref5));
		series1.getData().add(new XYChart.Data(preference6, pref6));
		series1.getData().add(new XYChart.Data(preference7, pref7));
		series1.getData().add(new XYChart.Data(preference8, pref8));
		series1.getData().add(new XYChart.Data(preference9, pref9));
		series1.getData().add(new XYChart.Data(preference10, pref10));
		series1.getData().add(new XYChart.Data(preferencenon, prefnon));

		// XYChart.Series series2 = new XYChart.Series();
		// series2.setName("Generic Algorithm");
		// series2.getData().add(new XYChart.Data(preference1, pref11));
		// series2.getData().add(new XYChart.Data(preference2, pref21));
		// series2.getData().add(new XYChart.Data(preference3, pref31));
		// series2.getData().add(new XYChart.Data(preference4, pref41));
		// series2.getData().add(new XYChart.Data(preference5, pref51));
		// series2.getData().add(new XYChart.Data(preference6, pref61));
		// series2.getData().add(new XYChart.Data(preference7, pref71));
		// series2.getData().add(new XYChart.Data(preference8, pref81));
		// series2.getData().add(new XYChart.Data(preference9, pref91));
		// series2.getData().add(new XYChart.Data(preference10, pref101));
		// series2.getData().add(new XYChart.Data(preferencenon, prefnon1));
		if (filepath != null) {
			bc.setAnimated(false);
		}
		Scene scene = new Scene(bc, 800, 600);
		bc.getData().addAll(series1);
		stage.setScene(scene);

		if (filepath == null) {
			stage.show();
		} else {
			WritableImage wim = new WritableImage(800, 600);
			scene.snapshot(wim);
			File file = new File(filepath + File.separator + System.currentTimeMillis() + "ChartImage.png");
			ImageIO.write(SwingFXUtils.fromFXImage(wim, null), "png", file);
			filepath = null;

		}

	}

}
